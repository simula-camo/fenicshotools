"FIXME"

from mpi4py import MPI as mpi
from fenics import *
from ..io import save_geometry, load_geometry
from .gmsh2dolfin import gmsh2dolfin
from .gmshfile import GmshFile
from .inline_backend import gmsh_cpp_geo2msh

import os
import tempfile
import shutil

__all__ = ["geo2dolfin"]

def geo2dolfin(code, topological_dimension=3, geometric_dimension=None,
               comm=None, marker_ids=None):
    """
    FIXME
    """

    comm = comm if comm is not None else mpi_comm_world()
    ilead = comm.tompi4py().rank == 0

    if ilead:
        # create a temporary directory
        tmpdir = tempfile.mkdtemp()

        # generates a new geo file
        geoname = os.path.join(tmpdir, 'mesh.geo')
        with open(geoname, 'w') as f:
            f.write(code)

        # generates the mesh
        info("--- Generating .msh file from .geo (may take a while)")
        mshname = os.path.join(tmpdir, 'mesh.msh')
        logname = os.path.join(tmpdir, 'mesh.log')
        curdir = os.getcwd()
        os.chdir(tmpdir)
        gmsh_cpp_geo2msh(geoname, topological_dimension, mshname, logname)
        os.chdir(curdir)

        # communicate the filename
        comm.tompi4py().bcast(mshname, root=0)
    else:
        # receive the filename
        mshname = comm.tompi4py().bcast(None, root=0)

    # import the mesh
    info("--- Importing from .msh")
    gmsh = GmshFile(mshname, geometric_dimension, get_log_level() == PROGRESS, marker_ids)
    domain, markers = gmsh2dolfin(gmsh, mpi_comm_self(), use_coords=True)

    # save it to hdf5
    if ilead:
        h5name = os.path.join(tmpdir, 'mesh.h5')
        save_geometry(mpi_comm_self(), domain, h5name, markers=markers)
        # communicate the filename
        comm.tompi4py().bcast(h5name, root = 0)
    else:
        h5name = comm.tompi4py().bcast(None, root = 0)

    # import the mesh in parallel
    return load_geometry(comm, h5name)

