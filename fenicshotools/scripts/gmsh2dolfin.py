#!/usr/bin/env python
from fenics import *
from fenicshotools.io import *
from fenicshotools.gmsh import GmshFile, gmsh2dolfin

import argparse
import os.path

def get_args():
    descr = 'Convert a .msh GMSH mesh file to a FEniCS .h5 file.'
    parser = argparse.ArgumentParser(description=descr)
    parser.add_argument('ifile', metavar='myfile.msh',
            type=str, 
            help='GMSH .msh filename.')
    parser.add_argument('-o', metavar='myfile.h5',
            type=str,
            help='FEniCS .h5 filename.')
    return parser

def main_func():

    comm = mpi_comm_world()
    args = get_args().parse_args()

    # 1. parsing
    gmsh = GmshFile(args.ifile)
    gmsh.print_info()

    # 2. conversion
    domain, markers = gmsh2dolfin(gmsh, comm, True)

    # 3. writing
    ofile = args.o or '{}.h5'.format(os.path.splitext(args.ifile)[0])
    save_geometry(comm, domain, ofile, markers=markers)

